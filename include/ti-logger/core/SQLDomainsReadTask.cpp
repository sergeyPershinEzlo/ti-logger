// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <sqlite3.h>

#include <ti-logger/core/Utils.h>
#include <ti-logger/core/DBManager.h>
#include <ti-logger/core/SQLDomainsReadTask.h>

using namespace ti;

void SQLDomainsReadTask::request(sqlite3* aDB)
{
	SQLResult result;
	
	sqlite3_stmt* aStatement = nullptr;

	auto& table = DBManager::kContentTableName;
	static const std::string kReq = "Select distinct(DOMAIN) from " + table + " WHERE DOMAIN<>''";
	
	result.Code = sqlite3_prepare_v2(aDB, kReq.c_str(), -1, &aStatement, 0);
	
	if(SQLITE_OK == result.Code)
	{
		int aResult = 0;
		while(true)
		{
			aResult = sqlite3_step(aStatement);
			
			if(aResult == SQLITE_ROW)
			{
				
				SQLDataRow drow = {
					(char*)sqlite3_column_text(aStatement, 0),
				};
				result.data.push_back(drow);
			}
			else
			{
				break;
			}
		}
		
		sqlite3_finalize(aStatement);
	}
	
	onCompletion(result);
}