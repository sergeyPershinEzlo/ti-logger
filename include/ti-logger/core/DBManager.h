// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <chrono>
#include <atomic>
#include <thread>
#include <sqlite3.h>

#include <ti-logger/core/Singleton.h>
#include <ti-logger/core/ISQLTask.h>
#include <ti-logger/core/ConcurrentQueue.h>
 
namespace  ti
{
    class DBManager : public Singleton<DBManager>
    {
    public:
        friend class Singleton;
		void init(const std::string& dataBaseFolderPath);
        
        void drop();
        
        void pushTask(std::shared_ptr<ISQLTask> task);
        
        static const std::string kContentTableName;
    private:
        DBManager();
        ~DBManager();
        
        bool hasTalble() const;
        void loop();
        void createTable();
        void deleteOldRecords();
        
        bool isTimeToClear();
        
        sqlite3* mDatabase;
        
        std::mutex mTableLock;
        std::atomic<bool> mDie;
        std::atomic<bool> mInitialized;
        conc::ConcurrentQueue<ISQLTask> mTasks;
        std::thread mWorker;
        
        std::chrono::time_point<std::chrono::system_clock> mLastClear;
    };
}
