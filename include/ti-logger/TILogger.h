// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <ti-logger/platform/Platform.h>

#if defined(__cplusplus)

#include <ti-logger/core/Log.h>
#include <ti-logger/core/Message.h>
#include <ti-logger/core/Listener.h>
#include <ti-logger/core/PrintListener.h>
#include <ti-logger/core/LocalListener.h>
#include <ti-logger/core/Utils.h>
#include <ti-logger/core/WEBListener.h>
#include <ti-logger/core/LANListener.h>

#endif

#if defined(TI_PLATFORM_IOS) || defined(TI_PLATFORM_MAC)

#if defined(__OBJC__)

#import <ti-logger/platform/ios/TILogInterface.h>
#import <ti-logger/platform/ios/TILog.h>

#endif

#endif
