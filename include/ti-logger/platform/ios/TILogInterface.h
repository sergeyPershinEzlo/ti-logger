// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#ifndef TIWRAPPER
#define TIWRAPPER

// define TILOG_DISABLE_LOGGING to prevent lib for affecting your app performance. You could do this in Release build scheme, for example.
#ifndef TILOG_DISABLE_LOGGING

#include <ti-logger/platform/ios/TIConstants.h>
#import <UIKit/UIKit.h>

#if defined __cplusplus
extern "C" {
#endif
    
void TILog_initIOS(NSString* apiKey);
void TILog_showLogInViewController(UIViewController* aViewController);
void TILog_showReportInViewController(UIViewController* aViewController);

void TILog_addListenerOfType(int aID);
void TILog_delListenerOfType(int aID);

void TILog_configureLANListener(NSString* const aHost, NSInteger aPort);
void TILog_configureWEBListener(NSString* const aHost, NSInteger aPort);

void TILog_info(NSString* const aDomain, NSString* const format, ...);
void TILog_info_ex(NSString* const aDomain, const NSArray* aTags, NSString* const format, ...);
void TILog_warning(NSString* const aDomain, NSString* const format, ...);
void TILog_warning_ex(NSString* const aDomain, const NSArray* aTags, NSString* const format, ...);
void TILog_error(int aCode,NSString* const aDomain, NSString* const format, ...);
void TILog_error_ex(int aCode, NSString* const aDomain, const NSArray* aTags, NSString* const aFormat, ...);
#if defined __cplusplus
};
#endif

#else

void TILog_initIOS(){};
void TILog_showLogInViewController(UIViewController* aViewController){};

void TILog_addListenerOfType(TIListenerID aID){}
void TILog_delListenerOfType(TIListenerID aID){}

void TILog_configureLANListener(NSString* const aHost, NSInteger aPort){};
void TILog_configureWEBListener(NSString* const aHost, NSInteger aPort){};

void TILog_info(NSString* const aDomain, NSString* const format, ...){};
void TILog_info_ex(NSString* const aDomain, const NSArray* aTags, NSString* const format, ...){};
void TILog_warning(NSString* const aDomain, NSString* const format, ...){};
void TILog_warning_ex(NSString* const aDomain, const NSArray* aTags, NSString* const format, ...){};
void TILog_error(int aCode,NSString* const aDomain, NSString* const format, ...){};
void TILog_error_ex(int aCode, NSString* const aDomain, const NSArray* aTags, NSString* const aFormat, ...){};
#endif

#endif