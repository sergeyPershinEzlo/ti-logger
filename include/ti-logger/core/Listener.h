// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#ifndef TI_LISTENER_H
#define TI_LISTENER_H
//#pragma once

namespace ti
{
    class Message;
    
    class Listener
    {
    public:
        virtual void acceptMessage (const Message& aMessage) = 0;
		virtual bool storesMessages() const { return false; }
    };
}

#endif