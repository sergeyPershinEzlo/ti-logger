// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#pragma once

#include <string>
#include <set>
#include <iostream>
#include <vector>

namespace ti
{
    class Message;
	class Utils
	{
    public:
        static std::string strFormat(const char* aFmt, ...);
        
        static std::string tagsToString (const std::set<int> aTags);
        static std::string toSQLRequest(const std::string& aTable, const Message& msg);
        static Message parseSQLResponseToMessage(std::string aLevel,
                                                 std::string aCode,
                                                 std::string aDomain,
                                                 std::string aTags,
                                                 std::string aDescription,
                                                 std::string aTimeStamp);
    };
	
    class Stopwatch
    {
    public:
        Stopwatch()
        {
            _tu_point = std::chrono::steady_clock::now();
        }
        
        void print(const std::string& tag)
        {
            auto now = std::chrono::steady_clock::now();
            std::chrono::milliseconds diff =
            std::chrono::duration_cast<std::chrono::milliseconds>(now - _tu_point);
            
            std::cout << (tag) <<" "<< diff.count() << " msec passed;\n";\
        }
        
    private:
        std::chrono::steady_clock::time_point _tu_point;
    };
}
