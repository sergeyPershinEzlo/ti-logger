// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#ifndef TI_SINGLETON_H
#define TI_SINGLETON_H

#include <mutex>
#include <assert.h>

namespace ti
{
    template<typename T>
    class Singleton
    {
    public:
        static T* instance();
        static void destroy();
        
    protected:
        inline explicit Singleton()
        {
            assert(Singleton::gInstance == 0);
            Singleton::gInstance = static_cast<T*>(this);
        }
        
        inline virtual ~Singleton()
        {
            Singleton::gInstance = 0;
        }
        
    private:
        static T* createInstance();
        static void destroyInstance(T*);
        
    private:
        static T* gInstance;
        
        
        
    private:
        inline explicit Singleton(Singleton const&) {}
        inline Singleton& operator=(Singleton const&) { return *this; }
    };

    template<typename T>
    T* Singleton<T>::instance()
    {
        static std::mutex gInstanceLock;
        if ( Singleton::gInstance == nullptr )
        {
            std::lock_guard<std::mutex> lock(gInstanceLock);
            Singleton::gInstance = createInstance();
        }
        return Singleton::gInstance;
    }

    template<typename T>
    void Singleton<T>::destroy()
    {
        if ( Singleton::gInstance != 0 )
        {
            destroyInstance(Singleton::gInstance);
            Singleton::gInstance = 0;
        }
    }

    template<typename T>
    inline T* Singleton<T>::createInstance()
    {
        return new T();
    }

    template<typename T>
    inline void Singleton<T>::destroyInstance(T* p)
    {
        delete p;
    }

    template<typename T>
    T* Singleton<T>::gInstance = 0;
}

#endif