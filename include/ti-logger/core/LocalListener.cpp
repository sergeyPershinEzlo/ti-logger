// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/LocalListener.h>

#include <iostream>
#include <vector>
#include <string>
#include <functional>
#include <memory>
#include <future>

#include <ti-logger/core/SQLWriteTask.h>
#include <ti-logger/core/DBManager.h>
#include <ti-logger/core/Message.h>

using namespace ti;

LocalListener::LocalListener() : mSending(false), mFlushPeriod(100), mCountPerTransaction(2), mMode(EDispatchMode::Sync)
{
    
}

LocalListener::~LocalListener()
{
    
}

void LocalListener::acceptMessage(const Message& aMessage)
{
    std::vector<Message> subVec;
    {
        std::lock_guard<std::mutex> lock(mMtx);
        mMessages.push_back(aMessage);
        if (isTimeToFlush())
        {
            for (int i = 0; i < mMessages.size(); ++i)
            {
                subVec.push_back(mMessages[i]);
            }
            
            mMessages.clear();
            mSending.store(true);
            
            auto sender = [subVec, this]{
                
                auto ptr = std::make_shared<SQLWriteTask>(std::move(subVec), [](SQLResult result){
                    
                    //std::cout<<"LocalListener::acceptMessage -> MSG saved with code "<<result.Code<<"\n";
                    
                });
                
                std::shared_ptr<ISQLTask> task = std::static_pointer_cast<ISQLTask>(ptr);
                DBManager::instance()->pushTask(task);
                this->mSending.store(false);
            };
            
            if (mMode.load() == EDispatchMode::Async)
            {
                std::thread(sender).detach();
            }
            else
            {
                sender();
            }
        }
        else return;
    }
}


void LocalListener::setMessagesCountPerTransaction(int aCount)
{
    mCountPerTransaction = aCount;
}

bool LocalListener::isTimeToFlush()
{
    unsigned cnt = mCountPerTransaction.load();
    bool flag = false;
    
    tMSec time = std::chrono::duration_cast<tMSec>(std::chrono::system_clock::now() - mLastFlush);
    
    flag = flag || (cnt >= mMessages.size());
    flag = flag || (time >= mFlushPeriod);
    flag = flag && !mMessages.empty();
    flag = flag && !mSending.load();
    
    if (flag)
    {
        mLastFlush = std::chrono::system_clock::now();
    }
    
    return flag;
}



















