// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.
#include <iostream>

#include <ti-logger/core/DispatcherThread.h>

using namespace ti;
static auto kSendTime = std::chrono::seconds(60);


DispatcherThread::DispatcherThread(fProcessCallback aCallback, int aMsgPerTransaction): 
cbProcess(aCallback),
kSleepTime(42),
mCntPerTransaction(aMsgPerTransaction)
{
    mState.exchange(EState::Sleeping);
    
    mWorker = std::thread(&DispatcherThread::dispatchLoop, this);
}

DispatcherThread::~DispatcherThread()
{
    mState.exchange(EState::Dying);
    mWorker.join();
}

void DispatcherThread::push(const ti::Message& aMsg)
{
    std::shared_ptr<Message> p;
    p = std::make_shared<Message>(aMsg);
    mQueue.push(aMsg);
}

void DispatcherThread::run()
{
    mState.exchange(EState::Running);
}

void DispatcherThread::stop()
{
    mState.exchange(EState::Sleeping);
}

void DispatcherThread::dispatchLoop()
{
    EState state = EState::Sleeping;
    
    while (EState::Dying != state)
    {
        state = mState.load();
        
        if (EState::Sleeping == state)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(kSleepTime));
        }
        else if (EState::Running == state)
        {
            std::vector<std::shared_ptr<Message>> messages;
            int cnt = mCntPerTransaction.load();
            messages.reserve(cnt);
            
            do
            {
                messages.push_back(mQueue.waitAndDequeue());
            }
            while (messages.size() < cnt && !timedOut());
            
            mLastClear = std::chrono::system_clock::now();
            cbProcess(std::move(messages));
        }
    }
#ifdef TI_LOG_ENABLED
    std::cout<<"DispatcherThread::dispatchLoop terminated";
#endif
}

bool DispatcherThread::timedOut()
{
    return (mLastClear - std::chrono::system_clock::now()) > kSendTime;
}













