// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#import <UIKit/UIKit.h>

#import <ti-logger/platform/ios/TILogVC.h>
#import <ti-logger/platform/ios/TIReportVC.h>
#import <ti-logger/platform/ios/TILog.h>
#import <ti-logger/platform/ios/NSString+Features.h>
#import <Parse/Parse.h>


#ifndef _ARG_STR

#define _ARG_STR(Format) va_list args; \
	NSString* f = Format ? Format : @""; \
	va_start(args, Format); \
	NSString *s = [[NSString alloc] initWithFormat:f arguments:args]; \
	va_end(args);
#endif


void TILog_configureLANListener(NSString* const aHost, NSInteger aPort)
{
    [TILog configureLANListener:aHost port:aPort];
}

void TILog_configureWEBListener(NSString* const aHost, NSInteger aPort)
{
    [TILog configureWEBListener:aHost port:aPort];
}

void TILog_info(NSString* const aDomain, NSString* const aFormat, ...)
{
	_ARG_STR(aFormat)
	[TILog info:aDomain tags:nil text:s];
}


void TILog_info_ex(NSString* const aDomain, const NSArray* aTags, NSString* const aFormat, ...)
{
	_ARG_STR(aFormat)
	[TILog info:aDomain tags:aTags text:s];
}

void TILog_warning(NSString* const aDomain, NSString* const aFormat, ...)
{
	_ARG_STR(aFormat)
	[TILog warning:aDomain tags:nil text:s];
}


void TILog_warning_ex(NSString* const aDomain, const NSArray* aTags, NSString* const aFormat, ...)
{
	_ARG_STR(aFormat)
	[TILog warning:aDomain tags:aTags text:s];
}


void TILog_error(int aCode, NSString* const aDomain, NSString* const aFormat, ...)
{
	_ARG_STR(aFormat)
	[TILog error:aCode domain:aDomain tags:nil text:s];
}


void TILog_error_ex(int aCode, NSString* const aDomain, const NSArray* aTags, NSString* const aFormat, ...)
{
	_ARG_STR(aFormat)
	[TILog error:aCode domain:aDomain tags:aTags text:s];
}


void TILog_showLogInViewController(UIViewController* aViewController)
{
	if (!aViewController)
	{
		return;
	}
	
	UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:[[TILogVC alloc] init]];
	[aViewController presentViewController:nc animated:YES completion:^{}];
}

void TILog_showReportInViewController(UIViewController* aViewController)
{
    if (!aViewController)
    {
        return;
    }
    
    UINavigationController* nc = [[UINavigationController alloc] initWithRootViewController:[[TIReportVC alloc] init]];
    [aViewController presentViewController:nc animated:YES completion:^{}];
}

void TILog_initIOS(NSString* apiKey)
{
    [Parse enableLocalDatastore];
    [Parse setApplicationId:@"KX0z9FXbI22hguZOv12yNl2wgs2E1d4UiAIuHvV1"
                  clientKey:@"RmQBLzbj4siy81UfLI8dWioHEJnLj9cbHAFQwo5j"];
    
    NSString* instanceIdString = [NSString new];
    instanceIdString = [instanceIdString generateRandomString:15];
    [TILog logInitIOS:apiKey instanceId:[instanceIdString MD5String]];
}

void TILog_addListenerOfType(int aID)
{
    [TILog addListener:aID];
}

void TILog_delListenerOfType(int aID)
{
    [TILog delListener:aID];
}


















