// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <stdio.h>
#include <sqlite3.h>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>

#include <ti-logger/core/Singleton.h>
#include <ti-logger/core/Listener.h>
#include <ti-logger/core/Log.h>

namespace ti
{
	class LocalListener : public Singleton<LocalListener>, public Listener
	{
	public:
        typedef std::chrono::milliseconds tMSec;
        
        enum class EDispatchMode
        {
            Sync,
            Async
        };
        
		LocalListener();
		virtual ~LocalListener();
        
        bool storesMessages() const override { return true; }
		void acceptMessage (const Message& aMessage) override;
        
        void setMessagesCountPerTransaction(int aCount);
        
        void setFlushTimeout(std::chrono::milliseconds aDuration)
        {
            std::lock_guard<std::mutex> lock(mMtx);
            mFlushPeriod = aDuration;
        }
        
        void setMode(EDispatchMode aMode)
        {
            mMode.store(aMode);
        }
        
    private:
        bool isTimeToFlush();
        
        std::vector<Message>    mMessages;
        std::mutex              mMtx;
        std::atomic<unsigned>   mCountPerTransaction;
        std::atomic<EDispatchMode> mMode;
        
        std::chrono::system_clock::time_point mLastFlush;
        tMSec                   mFlushPeriod;
        std::atomic_bool        mSending;
	};
}