// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/WEBListener.h>
#include <ti-logger/core/Message.h>
#include <ti-logger/core/Utils.h>
#include <ti-logger/core/DispatcherThread.h>
#include <ti-logger/core/HttpClient.h>

#include <string>
#include <json11/json11.hpp>

using namespace ti;

void WEBListener::init(const std::string& aHost, int aPort)
{
    WEBListener* intance = Singleton<WEBListener>::instance();
	delete intance->mHttpClient;
	intance->mHttpClient = new HttpClient(aHost, aPort);
    
	intance->mHost = aHost;
	intance->mPort = aPort;
    intance->mDispatcher->run();
}

void WEBListener::setMsgPerTransaction(int aVal)
{
    mDispatcher->setTransactionThreshold(aVal);
}

WEBListener::WEBListener() :
mHttpClient(nullptr)
{
    auto ptr = new DispatcherThread([this](std::vector<DispatcherThread::tMessageRef> messages){
        this->dispatch(messages);
    }, 15);
    
    mDispatcher = std::unique_ptr<DispatcherThread>(ptr);
}

bool WEBListener::canAccept(const Message& aMessage) const
{
	return true;
}

void WEBListener::acceptMessage(const Message& aMessage)
{
    assert(!instance()->mHost.empty()); // if been inited
    assert(instance()->mPort > 0); // if been inited
    
	mDispatcher->push(aMessage);
}

void WEBListener::dispatch(const std::vector<DispatcherThread::tMessageRef>& aMsgs)
{
    std::vector<json11::Json> root;;
    
    for(auto it: aMsgs)
    {
        it->PushSerialized(root);
    }
    json11::Json toJson = root;
    auto dump = toJson.dump();
    
	mHttpClient->submitRequest(dump.c_str(), dump.size());
}

WEBListener::~WEBListener()
{
	delete mHttpClient;
}







