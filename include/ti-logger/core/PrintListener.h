// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <ti-logger/core/Log.h>
#include <ti-logger/core/Listener.h>

namespace ti
{
	class Message;
	class PrintListener : public Listener, public Singleton<PrintListener>
	{
    public:		
		PrintListener(){}
		void acceptMessage (const Message& aMessage) override;
        void setSensitivity(LogLevel level){mSensitivity = level;};
        
    private:
        LogLevel mSensitivity = LogLevel::Error;
	};
}
