// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/PrintListener.h>

#include <iostream>
#include <ctime>

#include <ti-logger/core/Message.h>

using namespace ti;

void PrintListener::acceptMessage (const Message& aMessage)
{
	time_t t = static_cast<time_t>(aMessage.Timestamp);
	std::string msg;

	char buff[20];
	
	auto timeinfo = localtime (&t);
	strftime(buff, sizeof(buff), "%m.%d %R", timeinfo);
    
    if (aMessage.Level <= mSensitivity)
    {
        std::cout << buff << ": " << aMessage.Description.c_str() << std::endl;
    }
}