//
//  TILoggerIOS.h
//  TILoggerIOS
//
//  Created by Oleksander Mamchych on 3/17/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TILoggerIOS.
FOUNDATION_EXPORT double TILoggerIOSVersionNumber;

//! Project version string for TILoggerIOS.
FOUNDATION_EXPORT const unsigned char TILoggerIOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TILoggerIOS/PublicHeader.h>


