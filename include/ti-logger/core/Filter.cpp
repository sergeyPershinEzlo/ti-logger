// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/Filter.h>
#include <ti-logger/core/DBManager.h>
#include <iostream>

#define MICROSES 1000000
using namespace ti;


std::string Filter::getSQLRequest(LogLevel level, const std::string& aDomain, const std::set<int> aTags, long aLimit) const
{
	auto& table = DBManager::kContentTableName;
	
	bool anyLevel = level < LogLevel::Error || level > LogLevel::Info;
	std::string whereToken;
	if (!aDomain.empty() || !aTags.empty() || !anyLevel)
	{
		whereToken += " WHERE ";
		if (!anyLevel)
		{
			whereToken += " LEVEL=" + std::to_string(static_cast<int>(level));
		}
		if (!aDomain.empty())
		{
			if (!anyLevel)
			{
				whereToken += " AND";
			}
			whereToken += " DOMAIN='" + aDomain + "'";
		}		
		whereToken += " ";
	}
	
	const std::string limitToken = aLimit ? " LIMIT " + std::to_string(aLimit): "";
	auto ret = "SELECT * FROM " + table + whereToken + " ORDER BY TIMESTAMP desc" + limitToken + ";";
	std::cout << ret << "\n";
	return ret;

}


std::string Filter::getSQLRequest(LogLevel level, const std::string& aDomain, const std::set<int> aTags, long aLimit, time_t aTime) const
{
    auto& table = DBManager::kContentTableName;
    
    bool anyLevel = level < LogLevel::Error || level > LogLevel::Info;
    std::string whereToken;
    if (!aDomain.empty() || !aTags.empty() || !anyLevel || aTime > 0)
    {
        whereToken += " WHERE ";
        if (!anyLevel)
        {
            whereToken += " LEVEL=" + std::to_string(static_cast<int>(level));
        }
        if (!aDomain.empty())
        {
            if (!anyLevel)
            {
                whereToken += " AND";
            }
            whereToken += " DOMAIN='" + aDomain + "'";
        }
        
        if (aTime > 0)
        {
            if (!aDomain.empty())
            {
                whereToken += " AND";
            }
            
            std::time_t currentTime = std::time(nullptr);
            whereToken += " TIMESTAMP>=" + std::to_string((currentTime - aTime) * MICROSES);
        }
    }

    const std::string limitToken = aLimit ? " LIMIT " + std::to_string(aLimit): "";
    auto ret = "SELECT * FROM " + table + whereToken + limitToken + ";";
    std::cout << ret << "\n";
    return ret;
    
}


















