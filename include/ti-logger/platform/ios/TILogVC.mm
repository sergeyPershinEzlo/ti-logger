// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#import <MessageUI/MFMailComposeViewController.h>
#import "Objective-Zip.h"
#import <Parse/Parse.h>

#import <ti-logger/platform/ios/TILogVC.h>
#import <ti-logger/platform/ios/TILog.h>
#import <ti-logger/platform/ios/TIOBJMessage.h>
#import <ti-logger/platform/ios/TIDetailedLogVC.h>

#include <string>
#include <ti-logger/core/Filter.h>
#include <ti-logger/core/SQLReadTask.h>
#include <ti-logger/core/DBManager.h>
#include <ti-logger/core/Utils.h>
#include <json11/json11.hpp>
#include <ti-logger/core/JHelper.h>

@interface TILogVC ()<UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate>

@property (nonatomic, readwrite, weak) UITableView* table;

@property (nonatomic, readwrite, strong) UIBarButtonItem* sendButton;
@property (nonatomic, readwrite, strong) UIBarButtonItem* domainButton;
@property (nonatomic, readwrite, strong) UIBarButtonItem* levelButton;
@property (nonatomic, readwrite, strong) NSArray* messages;

@property (nonatomic, assign, readwrite) enum TIOBJLogLevel mLevel;
@property (nonatomic, strong, readwrite) NSString* mDomain;

@property (nonatomic, assign, readwrite) std::vector<ti::Message> cppMessages;

@end


static const CGFloat kTabBarHeight = 0.07f;

@implementation TILogVC

- (void)viewDidLoad {
	[super viewDidLoad];
	self.mDomain = @"";
	self.mLevel = TIOBJLogLevel::Any;
	
	UIBarButtonItem* leftb = [[UIBarButtonItem alloc] initWithTitle:@"Back"
										   style:UIBarButtonItemStylePlain
										  target:self
										  action:@selector(onBackPressed:)];
	self.navigationItem.leftBarButtonItem = leftb;
	
	UIBarButtonItem* rightB = [[UIBarButtonItem alloc] initWithTitle:@"Send"
															style:UIBarButtonItemStylePlain
														   target:self
														   action:@selector(onSendPressed:)];
    
    
    UIBarButtonItem* rightB2 = [[UIBarButtonItem alloc] initWithTitle:@"Send on server"
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(onSendOnServerPressed:)];
    
    
    self.navigationItem.rightBarButtonItems = @[rightB, rightB2];

	
    CGRect myBounds = self.view.bounds;
	UITableView* tv = [[UITableView alloc] initWithFrame:CGRectInset( CGRectMake(0, 0, myBounds.size.width, myBounds.size.height * (1.f - kTabBarHeight)), 0, 0)];
    
	self.table = tv;
	self.table.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin |
	UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
	UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	[self.view addSubview:self.table];
	
	self.table.delegate = self;
	self.table.dataSource = self;
	[self reloadData: TIOBJLogLevel::Any Domain:@""  tags:nil limit:0];
    
    UIToolbar* toolbar = [self createToolbar];
    CGFloat toolbarHeight = self.view.frame.size.height * kTabBarHeight;

    [self.view addSubview:toolbar];
    
    [self.view addConstraints:@[
    
                                [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:toolbar attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:0.0f],
                                [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:toolbar attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f],
                                [NSLayoutConstraint constraintWithItem:toolbar attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:toolbarHeight],
                                [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:toolbar attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]
                                
                                ]];
    
    toolbar.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view setNeedsLayout];

}


-(void) getMessages: (TIOBJLogLevel)level Domain:(NSString*)aDomain tags:(const NSArray*) aTags limit:(NSInteger)limit onComplete:(void(^)()) onComplete;
{

	std::string domain = aDomain ? [aDomain UTF8String] : "";
	
	auto filter = std::make_shared <ti::Filter>(static_cast<ti::LogLevel>(level), domain, std::set<int>(), limit);
	
	auto task = std::make_shared <ti::SQLReadTask>([self, onComplete](ti::SQLResult result){
		NSMutableArray* array = [[NSMutableArray alloc] init];
		auto messages = result.getMessages();
		self.cppMessages = messages;
		for (auto m : messages)
		{
			TIOBJMessage* mes = [TILog toObjCMessage: &m];
			[array addObject:mes];
		}
        
        NSArray* sortedArray = [array sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSNumber* first = [[NSNumber alloc] initWithLongLong:[(TIOBJMessage*)a Timestamp]];
            NSNumber* second = [[NSNumber alloc] initWithLongLong:[(TIOBJMessage*)b Timestamp]];
            return [second compare:first];
        }];
        
        self.messages = [sortedArray copy];
        
//        for(TIOBJMessage* hhh in self.messages)
//        {
//            NSLog(@"TimeStamp: %lld", hhh.Timestamp);
//        }
        
        onComplete();
	}, filter);
	
	
	ti::DBManager::instance()->pushTask(task);

}


-(void)reloadData: (TIOBJLogLevel)level Domain:(NSString*)domain tags:(const NSArray*) aTags limit:(NSInteger)limit
{

	
	void (^onComplete)() = ^()
	{
		
		dispatch_async(dispatch_get_main_queue(), ^{
			[self.domainButton setTitle: [NSString stringWithFormat: @"Domain: %@ ", [self.mDomain length] ? self.mDomain : @"ANY"]];
			[self.levelButton setTitle: [NSString stringWithFormat: @"Level: %@ ", [TIOBJMessage levelToString:self.mLevel]]];
			[self.table reloadData];
		});
	};
	
	[self getMessages:level Domain:domain tags:aTags limit:limit onComplete:onComplete];
}

- (UIToolbar*)createToolbar
{
	UIToolbar* toolbar = [[UIToolbar alloc] init];
    
	self.domainButton = [[UIBarButtonItem alloc] initWithTitle:@"Domain"
														 style:UIBarButtonItemStylePlain
														target:self
														action:@selector(onDomainClicked:)];
	self.levelButton = [[UIBarButtonItem alloc] initWithTitle:@"Level"
														style:UIBarButtonItemStylePlain
													   target:self
													   action:@selector(onLevelClicked:)];
	
	UIBarButtonItem* scrollButton = [[UIBarButtonItem alloc] initWithTitle:@"Scroll down"
																	 style:UIBarButtonItemStylePlain
																	target:self
																	action:@selector(scrollToBottom)];
	
	[toolbar setItems:@[self.domainButton, self.levelButton, scrollButton]];
	
	return toolbar;
}



typedef void(^ActionSheetBlock)(UIAlertAction*);
-(ActionSheetBlock)createDomainBlock :(NSString*)domain
{
	ActionSheetBlock block = ^(UIAlertAction* action){
		self.mDomain = domain;
		[self reloadData :self.mLevel Domain:self.mDomain tags:nil limit:500];
	};
	return block;
}
-(ActionSheetBlock)createLevelBlock :(TIOBJLogLevel)level
{
	ActionSheetBlock block = ^(UIAlertAction* action){
		self.mLevel = level;
		[self reloadData :self.mLevel Domain:self.mDomain tags:nil limit:500];
	};
	return block;
}


- (void)showDomainMenu:(NSArray*)domains
{
	UIAlertController* alertController =   [UIAlertController
								  alertControllerWithTitle:@"Pick domain"
								  message:@""
								  preferredStyle:UIAlertControllerStyleActionSheet];
	
	UIAlertAction* action = nil;
	
	
	for (NSString* s in domains) {
		action = [UIAlertAction actionWithTitle:s style:UIAlertActionStyleDefault handler: [self createDomainBlock :s]];
		[alertController addAction:action];
	}

	
	action = [UIAlertAction actionWithTitle:@"ANY" style:UIAlertActionStyleDefault handler: [self createDomainBlock :nil]];
	[alertController addAction:action];
	

	
	
	action = [UIAlertAction
			  actionWithTitle:@"Cancel"
			  style: UIAlertActionStyleCancel
			  handler:^(UIAlertAction * action)
			  {
				  [alertController dismissViewControllerAnimated:YES completion:nil];
			  }];
	[alertController addAction:action];
	
	[alertController.popoverPresentationController setPermittedArrowDirections:0];
	CGRect rect = self.view.frame;
	rect.origin.x = 0;
	rect.origin.y = 0;
	alertController.popoverPresentationController.sourceView = self.view;
	alertController.popoverPresentationController.sourceRect = rect;
	
	[self presentViewController:alertController animated:YES completion:nil];
}


- (void)onDomainClicked:(id)sender
{
	
	void (^onComplete)(NSArray*) = ^(NSArray* arr)
	{
		dispatch_async(dispatch_get_main_queue(), ^{
			[self showDomainMenu:arr];
		});
	};
	[TILog getDomains:onComplete]; 
}

- (void)onLevelClicked:(id)sender
{
	
	UIAlertController* alertController =   [UIAlertController
								  alertControllerWithTitle:@"Pick Level"
								  message:@""
								  preferredStyle: UIAlertControllerStyleActionSheet];

	UIAlertAction* action = nil;
	
	action = [UIAlertAction actionWithTitle:@"ANY" style:UIAlertActionStyleDefault handler: [self createLevelBlock :TIOBJLogLevel::Any]];
	[alertController addAction:action];
	
	action = [UIAlertAction actionWithTitle:@"ERROR" style:UIAlertActionStyleDefault handler: [self createLevelBlock :TIOBJLogLevel::Error]];
	[alertController addAction:action];
	
	action = [UIAlertAction actionWithTitle:@"WARNING" style:UIAlertActionStyleDefault handler: [self createLevelBlock :TIOBJLogLevel::Warning]];
	[alertController addAction:action];
	
	action = [UIAlertAction actionWithTitle:@"INFO" style:UIAlertActionStyleDefault handler: [self createLevelBlock :TIOBJLogLevel::Info]];
	[alertController addAction:action];
	
	action = [UIAlertAction actionWithTitle:@"DEBUG" style:UIAlertActionStyleDefault handler: [self createLevelBlock :TIOBJLogLevel::Debug]];
	[alertController addAction:action];
	
	
	action	=	[UIAlertAction
							 actionWithTitle:@"Cancel"
							 style: UIAlertActionStyleCancel
							 handler:^(UIAlertAction * action)
							 {
								 [alertController dismissViewControllerAnimated:YES completion:nil];
							 }];
	[alertController addAction: action];
	
	[alertController.popoverPresentationController setPermittedArrowDirections:0];
	CGRect rect = self.view.frame;
	rect.origin.x = 0;
	rect.origin.y = 0;
	alertController.popoverPresentationController.sourceView = self.view;
	alertController.popoverPresentationController.sourceRect = rect;
	
	
	[self presentViewController:alertController animated:YES completion:nil];
	
}


static NSString* const kTxtFileName = @"mailtext.txt";



- (void)onSendPressed:(id)sender
{
    NSString* apiKey = [TILog apiKey];
    NSString* instanceId = [TILog instanceId];
    NSString* deviceID = [UIDevice currentDevice].identifierForVendor.UUIDString;
    NSString* timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    NSString* appStateString = [NSString stringWithFormat:@"\"api_key\" : %@,\n \"instance_id\" : %@,\n \"device_id\" : %@,\n \"unix timestamp\" : %@\n", apiKey, instanceId, deviceID, timestamp];
    
    json11::Json root = jcpp::JHelper::vectorToJson(self.cppMessages);
    NSString* jsonString = [NSString stringWithFormat:@"%s", root.dump().c_str()];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"}," withString:@"},\n"];
    
    NSString* stringToSend = [NSString stringWithFormat:@"%@\n%@", appStateString, jsonString];
    [self sendMailWithMessageIfPossible:stringToSend];
}

- (void)onSendOnServerPressed:(id)sender
{
    json11::Json root = jcpp::JHelper::vectorToJson(self.cppMessages);
    NSString* jsonString = [NSString stringWithFormat:@"%s", root.dump().c_str()];
    jsonString = [jsonString stringByReplacingOccurrencesOfString:@"}," withString:@"},\n"];
    
    PFObject *statistics = [PFObject objectWithClassName:@"Statistics"];
    statistics[@"createdTime"] = [NSDate date];
    statistics[@"device_id"] = [[UIDevice currentDevice].identifierForVendor UUIDString];
    statistics[@"applicationInstanceId"] = [TILog instanceId];
    statistics[@"applicationKey"] = [TILog apiKey];
    statistics[@"errorLogs"] = jsonString;
    
    [statistics saveInBackground];
}

- (void)sendMailWithMessageIfPossible:(NSString*)message
{
    if (![MFMailComposeViewController canSendMail])
    {
        UIAlertController* actionSheet =   [UIAlertController
                                            alertControllerWithTitle:@"Can't send mail"
                                            message:@"You need to enable at least one mail account on the device"
                                            preferredStyle: UIAlertControllerStyleAlert];
        
        UIAlertAction* action  = [UIAlertAction
                                  actionWithTitle:@"Ok"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      [actionSheet dismissViewControllerAnimated:YES completion:nil];
                                  }];
        [actionSheet addAction: action];
        [self presentViewController:actionSheet animated:YES completion:nil];
        
    }
    else
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"Log Dump"];
        NSString* nonNilMessage = message == nil ? @"" : message;
        [controller setMessageBody:nonNilMessage isHTML:NO];
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller
		  didFinishWithResult:(MFMailComposeResult)result
						error:(nullable NSError *)error
{
	NSLog(@"RESULT %d Error %@", result, error);
	
	[controller dismissViewControllerAnimated:YES completion:^{
	}];
}



- (void)onBackPressed:(id)sender
{
	[self.navigationController dismissViewControllerAnimated:YES completion:^{}];
}


- (void)scrollToBottom
{
	const NSInteger cnt = self.messages.count;
	if (cnt <= 0)
	{
		return;
	}
	const NSInteger lastRow = cnt - 1;
	[self.table scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:lastRow inSection:0]
					  atScrollPosition:UITableViewScrollPositionBottom
							  animated:YES];
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if (indexPath.row >= self.messages.count)
	{
		return;
	}
	
	TIOBJMessage* message = self.messages[indexPath.row];
    NSMutableString* txt = [NSMutableString string];
    switch (message.Level)
    {
        case Error:
        {
            [txt appendFormat:@"ERR %@ @ %@\n", @(message.Code), message.Domain];
            break;
        }
        case Warning:
        {
            [txt appendFormat:@"WRN @ %@\n", message.Domain];
            break;
        }
        case Info:
        {
            [txt appendFormat:@"INF @ %@\n", message.Domain];
            break;
        }
        default:
        {
            [txt appendFormat:@"DBG @ %@\n", message.Domain];
            break;
        }
    }
    
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:0.000001 * message.Timestamp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-YYYY HH:mm:ss"];
    
    [txt appendFormat:@"%@ | %@\n", [dateFormatter stringFromDate:date], @(message.Timestamp)];
    if (message.Tags.count > 0)
    {
        [txt appendFormat:@"TAGS: %@\n", message.Tags];
    }
    [txt appendString:message.Description];

    TIDetailedLogVC* vc = [TIDetailedLogVC new];
    vc.errorText = txt;
    [self.navigationController pushViewController:vc animated:YES];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.messages.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString* const kCellID = @"logcell_id";
	UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellID];
	if (cell == nil)
    {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
									  reuseIdentifier:kCellID];
        cell.textLabel.font = [UIFont fontWithName:@"CourierNewPS-BoldMT" size:14.0f];
        cell.textLabel.numberOfLines = 2;
    }
	
	if (indexPath.row >= self.messages.count)
	{
		return cell;
	}
	
	TIOBJMessage* message = self.messages[indexPath.row];
    
    NSMutableString* txt = [NSMutableString string];
    
    switch (message.Level)
    {
        case Error:
        {
            [txt appendFormat:@"E %@ %@ ", @(message.Code), message.Domain];
            break;
        }
        case Warning:
        {
            [txt appendFormat:@"W %@ ", message.Domain];
            break;
        }
        case Info:
        {
            [txt appendFormat:@"I %@ ", message.Domain];
            break;
        }
        default:
        {
            [txt appendFormat:@"D %@ ", message.Domain];
            break;
        }
    }
    
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:0.000001 * message.Timestamp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    [txt appendFormat:@"%@\n", [dateFormatter stringFromDate:date]];
    [txt appendString:message.Description];
    
    static const NSInteger kMaxCellTextLength = 80;
    cell.textLabel.text = (txt.length > kMaxCellTextLength) ? ([txt substringToIndex:kMaxCellTextLength]) : (txt);
	
	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 40.0f;
}

@end
