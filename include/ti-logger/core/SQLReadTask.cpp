// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/SQLReadTask.h>

#include <sqlite3.h>

#include <ti-logger/core/Utils.h>
#include <ti-logger/core/DBManager.h>

using namespace ti;

void SQLReadTask::request(sqlite3* aDB)
{
    SQLResult result;
    
    sqlite3_stmt* aStatement = nullptr;
    result.Code = sqlite3_prepare_v2(aDB, mFilter->getSQLRequest().c_str(), -1, &aStatement, 0);
    
    if(SQLITE_OK == result.Code)
    {
        int aResult = 0;
        while(true)
        {
            aResult = sqlite3_step(aStatement);
            
            if(aResult == SQLITE_ROW)
            {
				
				SQLDataRow drow = {
					(char*)sqlite3_column_text(aStatement, 1),
					(char*)sqlite3_column_text(aStatement, 2),
					(char*)sqlite3_column_text(aStatement, 3),
					(char*)sqlite3_column_text(aStatement, 4),
					(char*)sqlite3_column_text(aStatement, 5),
					(char*)sqlite3_column_text(aStatement, 6)
				};
				result.data.push_back(drow);
            }
            else
            {
                break;
            }
        }
        
        sqlite3_finalize(aStatement);
    }
    
    onCompletion(result);
}



std::vector<Message> SQLResult::getMessages() const
{
	std::vector<Message> ret;
	for (SQLDataRow drow : data)
	{
		if (drow.size() >= 6)
		{
			auto message = Utils::parseSQLResponseToMessage(drow[0],drow[1], drow[2], drow[3], drow[4], drow[5]);
			ret.push_back(message);
		}
	}
	return ret;
}