// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <atomic>
#include <vector>
#include <functional>

#include <ti-logger/core/Message.h>
#include <ti-logger/core/ConcurrentQueue.h>

namespace ti
{
    class DispatcherThread
    {
    public:
        typedef std::shared_ptr<Message> tMessageRef;
        typedef std::function<void(std::vector<tMessageRef> messages)> fProcessCallback;
        
        void stop();
        void run();
        void setTransactionThreshold(int aTS)
        {
            mCntPerTransaction = aTS;
        }
        
        void push(const Message& aMsg);
        
        DispatcherThread(fProcessCallback aCallback, int aMsgPerTransaction);
        DispatcherThread() = delete;
        ~DispatcherThread();
    private:
        DispatcherThread(const DispatcherThread& aOther) = delete;
        DispatcherThread& operator=(const DispatcherThread& other) = delete;
        
        void dispatchLoop();
        
        bool timedOut();
        
        enum class EState
        {
            Running,
            Sleeping,
            Dying
        };
        
        const fProcessCallback cbProcess;
        
        conc::ConcurrentQueue<Message> mQueue;
        
        std::chrono::time_point<std::chrono::system_clock> mLastClear;
        
        const int           kSleepTime;
        std::atomic<int>    mCntPerTransaction;
        std::atomic<EState> mState;
        std::thread         mWorker;
    };
}





















