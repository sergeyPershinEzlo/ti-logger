//
//  TIDetailedLogVC.h
//  TundraIntegration
//
//  Created by Stanislav Bessonov on 3/15/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TIDetailedLogVC : UIViewController

@property (nonatomic, copy) NSString* errorText;
@end
