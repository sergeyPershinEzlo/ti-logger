// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/JHelper.h>

json11::Json jcpp::JHelper::vectorToJson(const std::vector<ti::Message>& vec)
{
    std::vector<json11::Json> dvec;
    
    for (auto it: vec)
    {
        dvec.push_back(it.jcpp::IJSerializable::Serialize());
    }
    
    return json11::Json(dvec);
}