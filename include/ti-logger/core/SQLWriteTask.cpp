// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/SQLWriteTask.h>

#include <sqlite3.h>

#include <ti-logger/core/Utils.h>
#include <ti-logger/core/DBManager.h>

using namespace ti;

SQLWriteTask::SQLWriteTask(const std::vector<Message>& aMessages, std::function<void(SQLResult)> aCb):
cbOnResult(aCb),
mMessages(aMessages)
{
    
}

void SQLWriteTask::request(sqlite3* aDB)
{
    SQLResult result;
    
    int code = SQLITE_OK;
    
    for (auto it: mMessages)
    {
        code = processRequest(aDB,
                       Utils::toSQLRequest(DBManager::kContentTableName, it));
        
        if (SQLITE_OK != code) break;
    }
    result.Code = code;
    onCompletion(std::move(result));
}

int SQLWriteTask::processRequest(sqlite3* aDB, const std::string& aRequest)
{
    char *zErrMsg = nullptr;
    const int rc = ::sqlite3_exec(aDB, aRequest.c_str(), nullptr, nullptr, &zErrMsg);
    if (rc != SQLITE_OK)
    {
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }
    
    return rc;
}