//
//  curlbuild.h
//  curl
//
//  Created by Pavel Yevtukhov on 19/02/16.
//  Copyright © 2016 Pavel Yevtukhov. All rights reserved.
//

#ifndef curlbuild_wrapper_h
#define curlbuild_wrapper_h

#ifdef IS_IOS
	#include "curlbuild_ios.h"
#else
	#include "curlbuild_osx.h"
#endif

#endif /* curlbuild_wrapper_h */
