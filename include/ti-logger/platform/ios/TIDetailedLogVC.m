//
//  TIDetailedLogVC.m
//  TundraIntegration
//
//  Created by Stanislav Bessonov on 3/15/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#import "TIDetailedLogVC.h"

@interface TIDetailedLogVC ()

@property (nonatomic, weak) UITextView* textView;

@end

@implementation TIDetailedLogVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setup];
}

- (void)setup
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIBarButtonItem* leftb = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                                              style:UIBarButtonItemStylePlain
                                                             target:self
                                                             action:@selector(onBackPressed:)];
    self.navigationItem.leftBarButtonItem = leftb;
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    UITextView* anchor = [[UITextView alloc] init];
    self.textView = anchor;
    self.textView.textColor = [UIColor blackColor];
    [self.textView setFrame:CGRectMake(0.0, 0.0,
                                       self.view.bounds.size.width,
                                       self.view.bounds.size.height)];
    [self.textView setAutoresizingMask:(UIViewAutoresizing)0x3F];
    
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.textColor = [UIColor blackColor];
    self.textView.text = self.errorText;
    self.textView.textAlignment = NSTextAlignmentLeft;
    self.textView.font = [UIFont fontWithName:@"CourierNewPS-BoldMT" size:10];
    self.textView.editable = NO;

    [self.view addSubview:self.textView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.textView.text = self.errorText;
    //[self textViewDidChange:self.textView];
    [self.textView setContentOffset:CGPointMake(0, 20) animated:NO];
}

- (void)onBackPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)textViewDidChange:(UITextView *)textView
{
    CGFloat fixedWidth = [textView.text sizeWithAttributes:textView.typingAttributes].width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    textView.contentSize = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
}

@end
