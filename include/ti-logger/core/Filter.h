// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <string>

#include <ti-logger/core/Message.h>

namespace ti
{

	class Filter
	{
	public:
		
		Filter() : Filter("")
		{}
		
		Filter(const std::string& aDomain, const std::set<int> aTags = std::set<int>(), long aLimit = 0)
		{
			mSqlRequest = getSQLRequest(static_cast<ti::LogLevel>(-1), aDomain, aTags, aLimit);
		}
		
		Filter(LogLevel level, const std::string& aDomain, const std::set<int> aTags, long aLimit = 0)
		{
			mSqlRequest = getSQLRequest(level, aDomain, aTags, aLimit);
		}
        
        Filter(LogLevel level, const std::string& aDomain, const std::set<int> aTags, long aLimit, time_t aTime)
        {
            mSqlRequest = getSQLRequest(level, aDomain, aTags, aLimit, aTime);
        }
        
		virtual ~Filter() {}

		std::string getSQLRequest() const { return mSqlRequest; };
			
    private:
		std::string mSqlRequest;
		std::string getSQLRequestAllDomains() const;
		std::string getSQLRequest(LogLevel level, const std::string& aDomain, const std::set<int> aTags, long aLimit) const;
        std::string getSQLRequest(LogLevel level, const std::string& aDomain, const std::set<int> aTags, long aLimit, time_t aTime) const;

	};
}