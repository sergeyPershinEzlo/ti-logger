// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <string>

#include <ti-logger/core/SQLResult.h>

class sqlite3;

namespace ti
{
    class ISQLTask
    {
    public:
        virtual void request(sqlite3* aDB) = 0;
        
    private:
        virtual void onCompletion(const SQLResult&) const = 0;
    };
}
