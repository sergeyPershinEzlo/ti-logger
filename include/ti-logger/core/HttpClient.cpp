// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/HttpClient.h>

#include <stdio.h>
#include <string>
#include <curl/curl.h>

using namespace ti;

HttpClient::HttpClient (const std::string& aHost, int aPort) : mHeaders(nullptr)
{
	mCurl = curl_easy_init();
	if(mCurl) {
		/*/api/log.php?device_id=...&app_id=...&instance_id=... , где device_id - идентификатор устройства, app_id - строка-идентификатор приложения и instance_id - timestamp запуска текущего процесса*/
		
		auto url = aHost + ":" +  std::to_string(aPort);
		curl_easy_setopt(mCurl, CURLOPT_URL, url.c_str());
		mHeaders = curl_slist_append(mHeaders, "Accept: application/json");
		mHeaders = curl_slist_append(mHeaders, "Content-Type: application/json");
		mHeaders = curl_slist_append( mHeaders, "charset:UTF-8");
		curl_easy_setopt(mCurl, CURLOPT_HTTPHEADER, mHeaders);
	}
}


HttpClient::~HttpClient ()
{
	curl_easy_cleanup(mCurl);
	curl_slist_free_all(mHeaders);
}

void HttpClient::submitRequest(const char * aRequest, size_t aSize)
{
    CURLcode res;
    //curl_easy_setopt(mCurl, CURLOPT_VERBOSE, 1L); uncomment to see logs
    curl_easy_setopt(mCurl, CURLOPT_POSTFIELDSIZE, aSize);
	curl_easy_setopt(mCurl, CURLOPT_POSTFIELDS, aRequest);
    curl_easy_setopt(mCurl, CURLOPT_TIMEOUT_MS, 500L);
	res = curl_easy_perform(mCurl);
	if(res != CURLE_OK)
	{
		//fprintf(stderr, "curl_easy_perform() failed: %s\n",	curl_easy_strerror(res));
	}
}