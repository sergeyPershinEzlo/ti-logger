//
//  TIAppInfoIOS.cpp
//  TundraIntegration
//
//  Created by Anton Komir on 28.01.16.
//  Copyright © 2016 Stanislav Bessonov. All rights reserved.
//

#include <ti-logger/platform/ios/TIAppInfoIOS.h>

#include <Foundation/Foundation.h>
//#import <UIKit/UIKit.h>

namespace ti {
	const char* AppInfoIOS::getAppID()
	{
		NSString* anAppID = [[NSBundle mainBundle] bundleIdentifier];
		return [anAppID UTF8String];
	}
	
	/*const char* AppInfoIOS::getDeviceID()
	{
		NSString* aDeviceID = [[UIDevice currentDevice] identifierForVendor].UUIDString;
		
		return [result UTF8String];
	}*/
}