// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/Message.h>

#include <stdio.h>
#include <time.h>

#include <ti-logger/core/Log.h>
#include <ti-logger/core/Listener.h>
#include <ti-logger/core/PrintListener.h>
#include <ti-logger/core/DBManager.h>
#include <ti-logger/core/SQLDeleteTask.h>

using namespace ti;

Log::Log() : mEnabled(true), mApiKeyString(""), mInstanceIdString("")
{ 
    addListener(PrintListener::instance());
}

void Log::init(const std::string& dbFolderPath, const std::string& apiKey, const std::string& instanceId)
{
    Log::instance()->setApiKey(apiKey);
    Log::instance()->setInstanceId(instanceId);
    DBManager::instance()->init(dbFolderPath);
    
    auto ptr = std::make_shared<SQLDeleteTask>([](SQLResult result){
        
        
    });
    
    //std::shared_ptr<ISQLTask> task = std::static_pointer_cast<ISQLTask>(ptr);
    DBManager::instance()->pushTask(ptr);
}

void Log::log(const Message& aMessage)
{
    if (!mEnabled.load()) return;
    
	if (aMessage.Domain.empty())
	{
		return;
	}
	mLock.lock();
	for (auto listener : mListeners)
	{
		listener->acceptMessage(aMessage);
	}
	mLock.unlock();
}

void Log::info(int aCode, const std::string& aDomain, const std::set<int> aTags, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Info,
                            aCode,
                            aDomain,
                            aTags,
                            aText));
#endif
}

void Log::info(const std::string& aDomain, const std::set<int> aTags, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Info,
                            aDomain,
                            aTags,
                            aText));
#endif
}

void Log::info(const std::string& aDomain, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Info,
                            aDomain,
                            aText));
#endif
}

void Log::warning(int aCode, const std::string& aDomain, const std::set<int> aTags, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Warning,
                            aCode,
                            aDomain,
                            aTags,
                            aText));
#endif
}

void Log::warning(const std::string& aDomain, const std::set<int> aTags, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Warning,
                            aDomain,
                            aTags,
                            aText));
#endif
}

void Log::warning(const std::string& aDomain, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Warning,
                            aDomain,
                            aText));
#endif
}

void Log::error(int aCode, const std::string& aDomain, const std::set<int> aTags, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Error,
                            aCode,
                            aDomain,
                            aTags,
                            aText));
#endif
}

void Log::error(int aCode, const std::string& aDomain, const std::string& aText)
{
#if TI_LOG_ENABLED == 1
    instance()->log(Message(LogLevel::Error, 
                            aDomain,
                            aText));
#endif
}

void Log::addListener(Listener* aListener)
{
	mLock.lock();
	
	auto it = std::find(mListeners.begin(), mListeners.end(), aListener);
	if (it == mListeners.end())
	{
		mListeners.push_back(aListener);
	}
	
	mLock.unlock();
}

void Log::removeListener(Listener* aListener)
{
	mLock.lock();
	
	auto it = std::find(mListeners.begin(), mListeners.end(), aListener);
	if (it != mListeners.end())
	{
		mListeners.erase(it);
	}
	
	mLock.unlock();
}

void Log::notify(const Message &aMessage, Listener* aListener)
{
	mLock.lock();
	
	auto it = std::find(mListeners.begin(), mListeners.end(), aListener);
	if (it != mListeners.end())
	{
		(*it)->acceptMessage(aMessage);
	}
	
	mLock.unlock();
}


void Log::removeAllListeners()
{
	mLock.lock();
    mListeners.clear();
    mListeners.push_back(PrintListener::instance());
	mLock.unlock();
}

void Log::setApiKey(std::string apiKey)
{
    mApiKeyString = apiKey;
}

void Log::setInstanceId(std::string instanceId)
{
    mInstanceIdString = instanceId;
}

std::string Log::getApiKey()
{
    return mApiKeyString;
}

std::string Log::getInstanceId()
{
    return mInstanceIdString;
}




