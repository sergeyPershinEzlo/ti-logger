// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#import <Foundation/Foundation.h>

@interface TIOBJMessage : NSObject

enum TIOBJLogLevel
{
    // should be equal to ti::LogLevel in c++
	Error, Warning, Info, Debug, Any
};

@property (nonatomic, assign, readonly) enum TIOBJLogLevel Level;
@property (nonatomic, assign, readonly) int				Code;
@property (nonatomic, copy, readonly) NSString*		Domain;
@property (nonatomic, copy, readonly) NSArray*		Tags;
@property (nonatomic, assign, readonly) long long       Timestamp;
@property (nonatomic, copy, readonly) NSString*		Description;


+(instancetype)init: (enum TIOBJLogLevel)level Code:(int)code Domain:(NSString*)domain Timestamp:(long long)timestamp Description:(NSString*)description;
-(NSString*)LevelStr;
+(NSString*)levelToString:(enum TIOBJLogLevel)level;

@end
