// Copyright  2016 Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <memory> 
#include <thread>
#include <mutex>

namespace conc
{
	template<typename T>
	class ConcurrentQueue
	{
	private:
		struct QueueNode 
		{
			std::shared_ptr<T> Data;
			std::unique_ptr<QueueNode> Next;
		};

		std::mutex _headMutex;
		std::mutex _tailMutex;

		std::unique_ptr<QueueNode> _head;
		QueueNode* _tail;

		std::condition_variable _condVariable;

		QueueNode* getTail()
		{
			std::lock_guard<std::mutex> tlock(_tailMutex);
			return _tail;
		}

		std::unique_ptr<QueueNode> popHead()
		{
			std::unique_ptr<QueueNode> old = std::move(_head);
			_head = std::move(old->Next);
			return old;
		}

		std::unique_lock<std::mutex> waitForElement()
		{
			std::unique_lock<std::mutex> hlock(_headMutex);
			_condVariable.wait(hlock, [&] {return _head.get() != getTail();});
			return std::move(hlock);
		}

		std::unique_ptr<QueueNode> waitForHead()
		{
			std::unique_lock<std::mutex> hlock(waitForElement());
			return popHead();
		} 

		std::unique_ptr<QueueNode> tryPopHead()
		{
			std::lock_guard<std::mutex> hlock(_headMutex);
			if (_head.get() == getTail())
			{
				return std::unique_ptr<QueueNode>();
			}
			return popHead();
		} 

		ConcurrentQueue& operator=(const ConcurrentQueue& other) = delete;
		ConcurrentQueue(const ConcurrentQueue& other) = delete;

	public: 
		ConcurrentQueue() :
			_head(new QueueNode), _tail(_head.get())
		{} 

		void push(T newValue)
		{
			std::shared_ptr<T> data(std::make_shared<T>(std::move(newValue))); 
			std::unique_ptr<QueueNode> p(new QueueNode);
			{
				std::lock_guard<std::mutex> tailLock(_tailMutex);
				_tail->Data = data;
				QueueNode* const newTail = p.get();
				_tail->Next = std::move(p);
				_tail = newTail;
			}
			_condVariable.notify_one();
		}
        
        void pushShared(std::shared_ptr<T> newValue)
        {
            std::shared_ptr<T> data(std::move(newValue));
            std::unique_ptr<QueueNode> p(new QueueNode);
            {
                std::lock_guard<std::mutex> tailLock(_tailMutex);
                _tail->Data = data;
                QueueNode* const newTail = p.get();
                _tail->Next = std::move(p);
                _tail = newTail;
            }
            _condVariable.notify_one();
        }

		std::shared_ptr<T> tryDequeue()
		{
			if (std::unique_ptr<QueueNode> old = tryPopHead())
			{
				return old;
			}
			return std::shared_ptr<T>();
		}

		std::shared_ptr<T> waitAndDequeue()
		{ 
			std::unique_ptr<QueueNode> const old = waitForHead();
			return old->Data;
		}

		bool empty()
		{
			std::lock_guard<std::mutex> hlock(_headMutex); 
			return (getTail() == _head.get());
		}
	};
}