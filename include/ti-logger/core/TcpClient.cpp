// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#include <ti-logger/core/TcpClient.h>

#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>

#include <stdio.h>


using namespace ti;

TcpClient::TcpClient (char* aLanIP, int aPort)
{
	mSocket = createSocket();
	setupConnection(mSocket);
	mLanIP = aLanIP;
	mPort = aPort;
}

TcpClient::~TcpClient ()
{
	close(mSocket);
}

void TcpClient::submitRequest(const char * aRequest)
{
	while (true) {
		if(send(mSocket , aRequest , strlen(aRequest) , 0) < 0)
		{
			puts("Reconnecting...");
			close(mSocket);
			mSocket = createSocket();
			setupConnection(mSocket);
		} else
		{
			break;
		}
	}
}

int TcpClient::createSocket()
{
	int aResultSocket;
	
	aResultSocket = socket(AF_INET , SOCK_STREAM , 0);
	if (aResultSocket == -1)
	{
		printf("Could not create socket");
	}
	puts("Socket created");
	
	int n = 1;
	setsockopt(aResultSocket, SOL_SOCKET, SO_NOSIGPIPE, &n, sizeof(n));
	
	
	return aResultSocket;
}

void TcpClient::setupConnection(int aSocket)
{
	struct sockaddr_in server;
	
	server.sin_addr.s_addr = inet_addr(mLanIP);
	server.sin_family = AF_INET;
	server.sin_port = htons(mPort);
	
	if (connect(aSocket , (struct sockaddr *)&server , sizeof(server)) < 0)
	{
		perror("connect failed. Error");
	}
	else
	{
		puts("Connected\n");
	}
	
}