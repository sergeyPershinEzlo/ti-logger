// Copyright  2016 TundraMobile - Vlad Joss. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <assert.h>
#include <string>
#include <memory> 
#include <vector>
#include <functional>

#include <ti-logger/core/Message.h>
#include <json11/json11.hpp>

namespace jcpp
{
	typedef std::function<void(const json11::Json& value)> cbArrayEnumerator;
	typedef std::function<void(const std::string& key, const json11::Json& value)> cbObjectEnumerator;

	class JHelper
	{
    public:
        static json11::Json vectorToJson(const std::vector<ti::Message>& vec);
		static void enumerate(const json11::Json& aRoot, cbArrayEnumerator aEnumerator)
		{
			assert(aRoot.is_array());
			
			auto vals = aRoot.array_items();
			for (auto item : vals)
			{
				aEnumerator(item);
			}
		}

		static void enumerate(const json11::Json& aRoot, cbObjectEnumerator aEnumerator)
		{
			assert(aRoot.is_object());

			for (auto kval : aRoot.object_items())
			{
				aEnumerator(kval.first, kval.second);
			}
		}
	};
}