// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <string>
#include <set>

#include <ti-logger/core/Log.h>
#include <ti-logger/core/IJSerializable.h>

namespace ti
{
    class Message : public jcpp::IJSerializable
	{
	public:
		Message(LogLevel Level,
				int aCode,
				const std::string& aDomain,
				const std::set<int>& aTags,
				long long aTs,
				const std::string& aDescription);
		
		Message(LogLevel Level,
				int aCode,
				const std::string& aDomain,
				const std::set<int>& aTags,
				const std::string& aDescription);
		
		Message(LogLevel Level,
				const std::string& aDomain,
				const std::set<int>& aTags,
				const std::string& aDescription);
		
		Message(LogLevel Level,
				const std::string& aDomain,
				const std::string& aDescription);
		
		Message& operator=(const Message& other);
		Message(const Message& aOther);
		
		~Message();
        
        virtual void Deserialize(const json11::Json& aNode) override;
        virtual void Serialize(json11::Json& aNode) override;
        
        virtual const std::string& getTag() override
        {
            static std::string tag = "message";
            return tag;
        };
        
		
		LogLevel        Level;
		int   Code;
		std::string     Domain;
		std::set<int>   Tags;
		long long       Timestamp;
		std::string     Description;
		
	private:
		static long long timestamp();
	};
}