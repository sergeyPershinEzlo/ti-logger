// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <ti-logger/core/Singleton.h>

#include <string>

struct curl_slist; 

namespace ti
{
	class HttpClient
    {
	public:
		~HttpClient();
		HttpClient(const std::string& aHost, int aPort);		
		void submitRequest(const char* request, size_t aSize);
    private:
		void* mCurl;
		struct curl_slist* mHeaders;
    };
}