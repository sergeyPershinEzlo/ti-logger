//
//  SQLDeleteTask.hpp
//  TundraIntegration
//
//  Created by Stanislav Bessonov on 3/18/16.
//  Copyright © 2016 Tundramobile. All rights reserved.
//

#pragma once

#include <functional>
#include <vector>
#include <string>
#include <ti-logger/core/Message.h>
#include <ti-logger/core/ISQLTask.h>
#include <ti-logger/core/SQLResult.h>

class sqlite3;
namespace ti
{
    class SQLDeleteTask : public ISQLTask
    {
    public:
        SQLDeleteTask(std::function<void(SQLResult)>);
        
        void request(sqlite3* aDB) override;
        
    private:
        void onCompletion(const SQLResult& aResult) const override
        {
            cbOnResult(aResult);
        }
        
        int processRequest(sqlite3* aDB, const std::string& aRequest);
        
        std::function<void(const SQLResult&)> cbOnResult;
    };
}
