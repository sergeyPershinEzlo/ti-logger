// Copyright  2016 TundraMobile. All rights reserved.
// This Source Code Form is subject to the terms of the Mozilla
// Public License, v. 2.0.If a copy of the MPL was not distributed
// with this file, You can obtain one at http ://mozilla.org/MPL/2.0/.

#pragma once

#include <ti-logger/core/Singleton.h>

namespace ti
{
	class TcpClient : public Singleton<TcpClient>
	{
	public:
		~TcpClient();
		TcpClient(char* aLanIP, int aPort);
		
		void submitRequest(const char* request);
	private:
		int createSocket();
		void setupConnection(int aSocket);
		
		int mSocket;
		char* mLanIP;
		int mPort;
	};
}